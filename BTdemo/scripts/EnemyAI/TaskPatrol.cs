using Godot;
using System;


using BehaviorTroll;

public partial class TaskPatrol : BtNode
{
	
	private Node2D[] _waypoints;
	private Node2D _position;
	private int _currentWaypointIndex = 0;
	private double Speed = 20;
	private double moveAmount = 1;
	
	public TaskPatrol(Node2D position, Node2D[] waypoints){
		this._waypoints = waypoints;
		this._position = position;
	}
	
	public override NodeState Evaluate(){
		
		Vector2 moveDirection = ( _waypoints[_currentWaypointIndex].Position - _position.Position).Normalized();
		// The normalized vector has length 1, but maintains direction towards the player.
		_position.Position += moveDirection * (float)moveAmount;
		
		if ( _position.Position.DistanceTo( _waypoints[_currentWaypointIndex].Position ) < 0.5f ) {
			GD.Print("moving to next waypoint.");
			_currentWaypointIndex++;
			if ( _currentWaypointIndex >= _waypoints.Length )
				_currentWaypointIndex = 0;
		}
		
		return NodeState.RUNNING;
	}

}
