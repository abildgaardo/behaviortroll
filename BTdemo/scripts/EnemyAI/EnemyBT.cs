using Godot;
using System;
using System.Collections.Generic;

using BehaviorTroll;

public partial class EnemyBT : BehaviorTroll.Tree
{
	[Export]
	Node2D[] waypoints;
	[Export]
	Node2D transform;
	[Export]
	Node2D player;
	private float _fovRange = 35;
	
	protected override BtNode SetupTree(){
		
		// BtNode root = new TaskPatrol(transform, waypoints);
		
	
		BtNode root = new Selector( new List<BtNode>
		{
			new Sequence(new List<BtNode>
			{
				new TaskHeroIsClose(player, transform, 120),
				new TaskWalkToTarget(player, transform)
			}),
			new TaskPatrol(transform, waypoints)
		});
		
		return root;

	}
}
