using Godot;
using System;

using BehaviorTroll;

public partial class TaskWalkToTarget : BtNode
{
	private Node2D _position;
	private Node2D _target;
	private float moveAmount = 1;
	
	public TaskWalkToTarget(Node2D target, Node2D position)
	{
		_position = position;
		_target = target;

	}

	public override NodeState Evaluate(){
		GD.Print("Walk to target.");
		Vector2 moveDirection = ( _target.Position - _position.Position ).Normalized();
		
		
		// Stop moving if close
		if ( _position.Position.DistanceTo( _target.Position ) > 0.5f ) {
			_position.Position += moveDirection * (float)moveAmount;
		}
		return NodeState.RUNNING;
	}
}
