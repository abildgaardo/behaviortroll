using Godot;
using System;

using BehaviorTroll;

public partial class TaskHeroIsClose : BtNode
{
	private Node2D _position;
	private Node2D _target;
	private float _range;
	
	public TaskHeroIsClose(Node2D target, Node2D position, float range)
	{
		_position = position;
		_target = target;
		_range = range;
	}

	public override NodeState Evaluate(){
		
		if ( _position.GlobalPosition.DistanceTo( _target.GlobalPosition ) < _range ) {
			return NodeState.SUCCESS;
		}	
		return NodeState.FAILURE;
	}
}
